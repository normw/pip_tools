#!/usr/bin/env python3

"""Match single-particle velocities and properties
"""

#--------------------------------------------------------------------------
# Match single-particle velocities and properties
#
# Copyright (c) 2019 Norman Wood
#
# This file is part of the free software PIP_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


"""This example code gets the velocities from the PIP_3 f_2_2_Velocity_Tables
files (both *v_1.dat and *v_2.dat), then finds the matching single particle
properties from the PIP_2 a_Particle_Tables files.  It outputs vertical
velocity as a function of particle size ('feret diameter' in PIP terms)
"""


from .context import pip_tools
import pip_tools.pip_f2
import pip_tools.pip_f22

import numpy
import glob

#
# Load the data
#

#Single particle tables
f2_files = glob.glob('examples/PIP_2_a_Particle_Tables/*.dat')
f2_files.sort()
single_particle_data = pip_tools.pip_f2.load(f2_files)

#Velocity v_2 tables
f22_v_2_files = glob.glob('examples/PIP_3_f_2_2_Velocity_Tables/*v_2.dat')
f22_v_2_files.sort()
velocity2_data = pip_tools.pip_f22.load(f22_v_2_files)

#Velocity v_1 tables
f22_v_1_files = glob.glob('examples/PIP_3_f_2_2_Velocity_Tables/*v_1.dat')
f22_v_1_files.sort()
velocity1_data = pip_tools.pip_f22.load(f22_v_1_files)

#
# Set up time intervals
#

#Set up the start times, end times and time intervals for the analysis
#Record numbers in PIP_2 a_Particle_Tables files are reset/restarted in
#10-minute blocks, so use a time inteval of 10 minutes and set the event
#start to 0, 10, 20, 30, 40, or 50 minutes after the hour.

event_start = numpy.datetime64('2016-12-11T00:00:00')
event_end = numpy.datetime64('2016-12-12T00:00:00')
time_interval = numpy.timedelta64(600, 's')
tbounds_start = numpy.arange(event_start, event_end, time_interval)

#
# Lists to save the output
#

diameter_list = []
vel_v_list = []

#
# Cycle over the time intervals and get the matched data
#

for tbound_start in tbounds_start:
    tbound_end = tbound_start + time_interval

    time_mask_single_particles = numpy.logical_and(tbound_start <= single_particle_data.times,
                                                   single_particle_data.times < tbound_end)

    
    time_mask_velocity2 = numpy.logical_and(tbound_start <= velocity2_data.times,
                                            velocity2_data.times < tbound_end)

    time_mask_velocity1 = numpy.logical_and(tbound_start <= velocity1_data.times,
                                            velocity1_data.times < tbound_end)


    sp_record_numbers = single_particle_data.record_number[time_mask_single_particles]
    sp_diameters = single_particle_data.max_feret_diam[time_mask_single_particles]
    sp_waddell_diams = single_particle_data.waddell_diam[time_mask_single_particles]

    #Process the velocity2 data for this time interval
    v2_record_numbers = velocity2_data.record_number[time_mask_velocity2]
    vel_vs = velocity2_data.vel_v[time_mask_velocity2]
    v2_waddell_diams = velocity2_data.waddell_diam[time_mask_velocity2]
    for i_record, v2_record_number in enumerate(v2_record_numbers):
        selection_mask = numpy.equal(sp_record_numbers, v2_record_number)
        if numpy.sum(selection_mask) == 1:
            #Got a match - this should always happen
            vel_v = vel_vs[i_record]
            v2_waddell_diam = v2_waddell_diams[i_record]
            diameter = sp_diameters[selection_mask][0]
            sp_waddell_diam = sp_waddell_diams[selection_mask][0]
            assert numpy.isclose(v2_waddell_diam, sp_waddell_diam)
            diameter_list.append(diameter)
            vel_v_list.append(vel_v)

    #Process the velocity1 data for this time interval
    v1_record_numbers = velocity1_data.record_number[time_mask_velocity1]
    vel_vs = velocity1_data.vel_v[time_mask_velocity1]
    v1_waddell_diams = velocity1_data.waddell_diam[time_mask_velocity1]
    for i_record, v1_record_number in enumerate(v1_record_numbers):
        selection_mask = numpy.equal(sp_record_numbers, v1_record_number)
        if numpy.sum(selection_mask) == 1:
            #Got a match - this should always happen
            vel_v = vel_vs[i_record]
            v1_waddell_diam = v1_waddell_diams[i_record]
            diameter = sp_diameters[selection_mask][0]
            sp_waddell_diam = sp_waddell_diams[selection_mask][0]
            assert numpy.isclose(v1_waddell_diam, sp_waddell_diam)
            diameter_list.append(diameter)
            vel_v_list.append(vel_v)

for x in zip(diameter_list, vel_v_list):
    print('%15.5e %15.5e' %(x[0], x[1]))
