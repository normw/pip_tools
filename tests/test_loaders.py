#!/usr/bin/env python3



"""Example test script
"""

#--------------------------------------------------------------------------
# Example test script
#
# Copyright (c) 2019 Norman Wood
#
# This file is part of the free software PIP_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


from .context import pip_tools
import pip_tools.pip_f2
import pip_tools.pip_f22

import numpy

single_particle_data = pip_tools.pip_f2.load(['tests/0072016121101200_a_p.dat',])
N_particle_obs = single_particle_data.record_number.shape[0]
velocity1_data = pip_tools.pip_f22.load(['tests/0072016121101200_a_v_1.dat',])
velocity2_data = pip_tools.pip_f22.load(['tests/0072016121101200_a_v_2.dat',])
N_velocity1_obs = velocity1_data.record_number.shape[0]
N_velocity2_obs = velocity2_data.record_number.shape[0]


for i_p, vel_particle_id in enumerate(velocity2_data.particle_id):
    #print(particle_id, velocity2_data.vel_v[i_p])

    mask = numpy.equal(single_particle_data.record_number, vel_particle_id)
    if numpy.sum(mask) == 1:
        D = single_particle_data.max_feret_diam[mask]
        print(single_particle_data.max_feret_diam[mask][0], velocity2_data.vel_v[i_p])
