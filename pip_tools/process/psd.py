#!/usr/bin/env python3

"""Processing for Precipitation Imaging Package data structures
"""

#--------------------------------------------------------------------------
# Processing for Precipitation Imaging Package data structures
#
# Copyright (c) 2009, 2010, 2017 Norman Wood
#
# This file is part of the free software PIP_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


# Notes
#######
#
# See notes from 16-19 Oct 2009, form of calculation for uncertainty
# in N(D) that takes into account Np_tot multiple independent
# measurements of 1/(AL)
#
#26 June 2010
# See revised notes that discuss both measurement and sampling error
# including notes of 19 May covering my discussion with Phil Chapman
# in the statistics department and my notes from 22 June 2010 - 26 June
# 2010 developing the methods for calculating each
# The original uncertainty model based on my 16-19 Oct 2009 notes has
# been revised to:
#   1) calculate measurement and sampling uncertainties separately,
#      then combine them into total uncertainty
#   2) include the proper treatment of the sampling uncertainty using
#      a randomly stopped sum

# 19 Nov 2010
# There was an error in the calculation of total_N that was causing total_N
# to be overestimated by a factor of 5.  In turn this was causing the uncertainties
# for N(D) to be overestimated, and the uncertainties for D to be underestimated.
# This was corrected as of this date.

import numpy
import scipy.odr
from .. import psddata

_MISSING_FLOAT = -999.
_MISSING_INT = -999

# Functions needed for variance calculations:
#
#   X_Df(X_fov, Y_fov, F_dof, D_vals)
#   dX_df(X_fov, Y_fov, F_dof, D_vals)
#   dX_dD(X_fov, Y_fov, F_dof, D_vals)
#   d2X_dD2(X_fov, Y_fov, F_dof, D_vals)
#
# where:
#   X_fov:  width of camera field of view
#   Y_fov:  height of camera field of view
#   F_dof:  factor for estimating depth of field
#   D_vals: particle sizes
#
#   X_fov, Y_fov and D_vals must be in the same length units


def X_Df(X_fov, Y_fov, F_dof, D_vals):
   width_1 = X_fov - D_vals
   width_2 = Y_fov - D_vals
   idx_bad_vol = numpy.where(numpy.logical_or(width_1 < 0., width_2 < 0.))[0]

   V_i = width_1*width_2*F_dof*D_vals
   X_i = 1./V_i
   X_i[idx_bad_vol] = 0.
   return X_i

def dX_df(X_fov, Y_fov, F_dof, D_vals):   #X_fov, Y_fov and D_vals must be in the same units
   width_1 = X_fov - D_vals
   width_2 = Y_fov - D_vals
   idx_bad_vol = numpy.where(numpy.logical_or(width_1 < 0., width_2 < 0.))[0]

   V_i = width_1*width_2*F_dof*D_vals
   X_i = 1./V_i
   deriv = -X_i/F_dof
   deriv[idx_bad_vol] = 0.

   return deriv

def dX_dD(X_fov, Y_fov, F_dof, D_vals):   #X_fov, Y_fov, and D_vals must be in the same units
   width_1 = X_fov - D_vals
   width_2 = Y_fov - D_vals
   idx_bad_vol = numpy.where(numpy.logical_or(width_1 < 0., width_2 < 0.))[0]

   V_i = width_1*width_2*F_dof*D_vals
   X_i = 1./V_i
   deriv = X_i*(1./width_1 + 1./width_2 - 1./D_vals)
   deriv[idx_bad_vol] = 0.
   return deriv

def d2X_dD2(X_fov, Y_fov, F_dof, D_vals):   #X_fov, Y_fov, and D_vals must be in the same units
   width_1 = X_fov - D_vals
   width_2 = Y_fov - D_vals
   idx_bad_vol = numpy.where(numpy.logical_or(width_1 < 0., width_2 < 0.))[0]

   V_i = width_1*width_2*F_dof*D_vals
   X_i = 1./V_i

   dXi_dD = dX_dD(X_fov, Y_fov, F_dof, D_vals)

   deriv = dXi_dD*(1./width_1 + 1./width_2 - 1./D_vals) + X_i*(1./(width_1*width_1) + 1./(width_2*width_2) + 1./(D_vals*D_vals))
   deriv[idx_bad_vol] = 0.
   return deriv


# Variance calculations

def ND_meas_var(X_fov, Y_fov, F_dof, Di_m, delta_Di_mm, Npi, lambda_mm, total_frames):
   """Calculate and return measurement variances

   Arguments:
   X_fov:        field of view width, m
   Y_fov:        field of view height, m
   F_dof:        factor for estimating depth of field, unitless
   Di_m:         nominal particle diameters for each bin, m
   delta_Di_mm:  bin widths, mm (can be scalar if uniform bin widths)
                 (note that units of mm will cause the variance to be in m^-6 mm^-2,
                  so that uncertainty is in m^-3 mm^-1)
   Npi:          number of particles counted for each bin
   lambda_mm:    slope of distribution in mm^-1
   """

   #Get the expected values of Di
   exp_term = numpy.exp(-lambda_mm*delta_Di_mm)
   lambda_m = lambda_mm*1000.
   delta_Di_m = delta_Di_mm*1.0e-3
   Di_expect_m = Di_m + 1./lambda_m - delta_Di_m*exp_term/(1. - exp_term)

   #X_fov, Y_fov, Di_m are all expected to be in meters
   #delta_Di_mm is expected to be in mm
   #Per Newman et al., 2009
   #Page 173
   F_dof_uncert = 0.15*F_dof

   #Pages 172 and 174 - includes errors due to image processing and due to
   #blurring when particles are outside the focal plane, treated as independent
   #errors
   Di_uncert_m = numpy.sqrt(Di_expect_m*0.18*Di_expect_m*0.18 + 0.2e-3*0.2e-3)

   dXi_df = dX_df(X_fov, Y_fov, F_dof, Di_expect_m)
   dXi_dD = dX_dD(X_fov, Y_fov, F_dof, Di_expect_m)

   var_Xi = dXi_dD*dXi_dD*Di_uncert_m*Di_uncert_m + dXi_df*dXi_df*F_dof_uncert*F_dof_uncert

   variance = Npi*var_Xi/(delta_Di_mm*delta_Di_mm*total_frames*total_frames)

   return variance


def ND_samp_var(X_fov, Y_fov, F_dof, Di_m, delta_Di_mm, Npi, lambda_mm, total_frames):
   """Calculate and return sample variances

   Arguments:
   X_fov:        field of view width, m
   Y_fov:        field of view height, m
   F_dof:        factor for estimating depth of field, unitless
   Di_m:         nominal particle diameters for each bin, m
   delta_Di_mm:  bin widths, mm (can be scalar if uniform bin widths)
                 (note that units of mm will cause the variance to be in m^-6 mm^-2,
                  so that uncertainty is in m^-3 mm^-1)
   Npi:          number of particles counted for each bin
   lambda_mm:    slope of distribution in mm^-1
   """

   #Get the expected values of Di
   exp_term = numpy.exp(-lambda_mm*delta_Di_mm)             #unitless
   lambda_m = lambda_mm*1000.
   delta_Di_m = delta_Di_mm*1.0e-3
   Di_expect_m = Di_m + 1./lambda_m - delta_Di_m*exp_term/(1. - exp_term)

   #Get the variance
   Di_var_m2 = 1./(lambda_m*lambda_m) - delta_Di_m*delta_Di_m*exp_term/((1. - exp_term)*(1. - exp_term))

   dXi_dD = dX_dD(X_fov, Y_fov, F_dof, Di_expect_m)            #m^-4
   var_Xi = dXi_dD*dXi_dD*Di_var_m2                            #m^-8*m^2 = m^-6

   #Now need the expected values of X
   #First get the 2nd derivative evaluated at expected values of D
   d2Xi_dD2_expect_D = d2X_dD2(X_fov, Y_fov, F_dof, Di_expect_m)  #m^-5
   #Now get X evaluated at the expected values of D
   X_D_expect_D = X_Df(X_fov, Y_fov, F_dof, Di_expect_m)      #m^-3
   #Finally use these to get the expected value of X
   expect_Xi = X_D_expect_D + d2Xi_dD2_expect_D*Di_var_m2/2.      #m^-3 + m^-5*m^-2 = m^-3

   variance = (Npi*var_Xi + Npi*expect_Xi*expect_Xi)/(delta_Di_mm*delta_Di_mm*total_frames*total_frames)  #m^-6 mm^-2
   return variance


def sample_volumes(psd_data):

    """Calculate PIP sample volumes

    Sample volumes in m^3 per frame for each PIP size bin

    Arguments:
    psd_data:  A PSDData instance (see pip_f13 module)
    """

    fov_width_m = (psd_data.fov_x - psd_data.bin_lbounds)*1.0e-3
    fov_height_m = (psd_data.fov_y - psd_data.bin_lbounds)*1.0e-3
    depth_of_field_m = psd_data.f_dof*psd_data.bin_lbounds*1.0e-3

    volume_per_frame = fov_width_m*fov_height_m*depth_of_field_m
    mask_bad_volumes = numpy.logical_or(fov_width_m <= 0.,
                                        fov_height_m <= 0.)
    volume_per_frame[mask_bad_volumes] = 0.

    return volume_per_frame


def fitfunc(B, D_vals):
   #B[0] = log(N0), B[1] = lambda
   return B[0] - B[1]*D_vals

def resample(psd_data, t_interval=None, t_start=None, t_end=None):
    #see original source in /data/Snow_retrieval_project/C3VP/Observations/SVI_newest/SVI_tools

    """Resample PIP/PVI/SVI psd data to desired time intervals

    For each interval, resample() resamples the pip data to the
    desired time intervals, calculating the mean size distribution
    along with the uncertainties in the bin sizes and in the size
    distribution.

    Arguments:
    psd_data:    A PSDData namedtuple (see pip_f13 module)
    t_interval:  Time interval, numpy.timedelta64, seconds
    t_start:     Start time, numpy.datetime64
    t_end:       End time, numpy.datetime64

    Returns:
    sample_start_times:  Starting times of the intervals, numpy.datetime64
    Di_uncert:           For each interval, uncertainties in D for each bin, mm
    N_Di_mean:           For each interval, means of N(D) for each bin, m^-3 mm^-1
    N_Di_uncert:         For each interval, uncertainties of N(D) for each bin, m^-3 mm^-1
    """

    if t_start == None:
        t_start = psd_data.times_utc[0]
    if t_end == None:
        t_end = psd_data.times_utc[-1]
    if t_interval == None:
        t_interval = psd_data.dt

    #Get the number of intervals to be processed
    N_intervals = ((t_end - t_start)/t_interval).astype('int')

    #Predefine results arrays.  These hold the results that are determined for
    #each sampled time interval.
    sample_start_times = numpy.zeros((N_intervals,), dtype=psd_data.times_utc[0].dtype)
    Di_uncert = numpy.zeros((N_intervals, psd_data.n_bins,), dtype=float)
    N_Di_mean = numpy.zeros((N_intervals, psd_data.n_bins,), dtype=float)
    N_Di_uncert = numpy.zeros((N_intervals, psd_data.n_bins,), dtype=float)
    lambda_mm_fitted = numpy.zeros((N_intervals,), dtype=float)
    N0_mm_fitted = numpy.zeros((N_intervals,), dtype=float)

    #Default missing value array (used for assigment to some diagnostic arrays
    missing_vals = numpy.ones((psd_data.n_bins,), dtype=float)*(_MISSING_FLOAT)

    #Assign psd_data to local variables for convenience
    N_bins = psd_data.n_bins
    Di_mm = psd_data.bin_lbounds
    delta_Di_mm = psd_data.bin_widths
    N_Di = psd_data.psd
    fov_xdim = psd_data.fov_x*1.0e-3
    fov_ydim = psd_data.fov_y*1.0e-3
    F_dof = psd_data.f_dof
    fps = psd_data.fps
    obs_duration_s = psd_data.dt
    
    #Number of frames contained in each reported observation
    frames_per_obs = (psd_data.fps*psd_data.dt/psd_data.frame_sampling_interval).astype(float)


    #For uncertainties, need disdrometer sample volumes in m^3 so that 
    #actual particle counts can be estimated.  See Newman et al., 2009
    Di_m = Di_mm*1.0e-3                                     #m
    fov_width = (fov_xdim - Di_m)                           #m
    fov_height = (fov_ydim - Di_m)                          #m
    depth_of_field = F_dof*Di_m                             #m
    volume_per_frame = fov_width*fov_height*depth_of_field  #m^3
    #Zero out the negative sample volumes
    idx_bad_vol = numpy.where(numpy.logical_or(fov_width <= 0.,
                                               fov_height <= 0.))[0]
    volume_per_frame[idx_bad_vol] = 0.


    #Needed for size distribution fitting to determine lambda
    exponential = scipy.odr.Model(fitfunc)

    #Step through time intervals and calculate uncertainties for the sample
    #of observations in each interval
    for i_interval, t_lower in enumerate(numpy.arange(t_start, t_end, t_interval)):
        t_upper = t_lower + t_interval

        #Skip any incomplete interval at the end
        if t_upper > t_end:
            continue

        #Record the time of this sample
        sample_start_times[i_interval] = t_lower

        #Mask to select observations reported within the time interval
        mask_t = numpy.logical_and(numpy.less_equal(t_lower, psd_data.times_utc),
                                   numpy.less(psd_data.times_utc, t_upper))

        N_obs = numpy.sum(mask_t)

        #Do calcs if this is a nonempty sample
        if N_obs > 0:

            #The number of observations within the interval, N_obs, may be less
            #than would be expected given t_interval and obs_duration_s 
            #because the PIP omits reporting size distributions for which 
            #N(D) = 0.  We need to count those omitted distributions, though,
            #when averaging given beginning and ending times and the interval
            #between the observations.
            #This is the actual sample size:

            #N_obs_actual = ((t_upper - t_lower).astype('m8[s]')/obs_duration_s)
            N_obs_actual = (t_upper - t_lower)/psd_data.dt

            #The N_Di over the entire set of samples would just be the sum of
            #all the volume inverses divided by the total number of frames 
            #divided by the bin width.  This gives the same result.
            N_Di_sample = N_Di[mask_t]
            N_Di_sum = numpy.sum(N_Di[mask_t], axis=0)
            print(N_Di.shape, N_Di_sample.shape, N_Di_sum.shape)
            N_Di_mean[i_interval, :] = N_Di_sum/N_obs_actual

            #Do an estimate of the variance of N_Di over the defined interval.
            #This is needed for fitting lambda and can be compared against the
            #modeled uncertainty.
            N_Di_var = (numpy.sum(numpy.power(N_Di[mask_t] - N_Di_mean[i_interval,:], 2), axis=0)
                        / (N_obs_actual - 1))

            
            #Modeling the uncertainties
            ###########################

            #This is the number per bin of particles totalled over all frames
            #for all the chosen samples.  Note the use of N_Di_sum
            #(=numpy.sum(N_Di[mask_t], axis=0)).  Since we're using the
            #summed N(Di), it's appropriate to use frames_per_obs in the
            #calculations.
            Nptot_i = N_Di_sum*delta_Di_mm*frames_per_obs*volume_per_frame

            #Identify the bins for which the average N_Di is positive and
            #the associated Di is large enough that the data are valid.
            mask_valid = numpy.logical_and(numpy.greater(N_Di_mean[i_interval, :], 0.),
                                           numpy.greater_equal(Di_mm, 0.25))
            N_bins_valid = numpy.sum(mask_valid)
      
            #Estimate the uncertainty in the Di associated with each bin where
            #the average N_Di is not zero.  Use mask_valid to select these bins.
            #The sampling variance is insigificant; this estimate is based purely on
            #the measurement variance.
            Di_uncert[i_interval, :] = 0.
            Di_uncert[i_interval, mask_valid] = (numpy.sqrt(
                                                     numpy.power(0.18*Di_mm[mask_valid], 2)
                                                   + 0.2e-3**2) 
                                                   / numpy.sqrt(Nptot_i[mask_valid]))

            #To get the ND sampling variance, need an estimate of lambda
            #Do this using the subset of bins selected by mask_valid
            N_Di_mean_sub = N_Di_mean[i_interval, mask_valid]
            log_N_Di_mean_sub = numpy.log(N_Di_mean_sub)
            N_Di_var_sub = N_Di_var[mask_valid]
            Di_mm_sub = Di_mm[mask_valid]
            #Equally weight the observations for this calculation
            we_vals_ones = numpy.ones((N_bins_valid,), dtype=float)
            fitdata = scipy.odr.Data(Di_mm_sub, log_N_Di_mean_sub, wd=0., we=we_vals_ones)
            myodr = scipy.odr.ODR(fitdata, exponential, beta0=[1000., 1.])
            myodr.set_job(fit_type=2)
            results = myodr.run()
            ier = results.info
            #compute a normed chi-sq statistic
            normed_chi_sq = numpy.sum(results.eps*results.eps/N_Di_var_sub)/N_bins_valid
            if ier < 4:   #Probably good convergence, see ODRPACK user guide, page 38
               total_frames = frames_per_obs*N_obs_actual
               (N0_mm_est, lambda_mm_est) = results.beta
               sampling_var_est = ND_samp_var(fov_xdim, fov_ydim, F_dof, Di_m, delta_Di_mm, Nptot_i, lambda_mm_est, total_frames)
               measurement_var_est = ND_meas_var(fov_xdim, fov_ydim, F_dof, Di_m, delta_Di_mm, Nptot_i, lambda_mm_est, total_frames)
               N_Di_uncert[i_interval, :] = numpy.sqrt(measurement_var_est + sampling_var_est)
               lambda_mm_fitted[i_interval] = lambda_mm_est
               N0_mm_fitted[i_interval] = N0_mm_est
               #Just for diagnostic output
               total_var_est = sampling_var_est + measurement_var_est
            else:
               #Can't estimate lambda for some reason, most likely insufficient data such as when only one size bin is occupied
               #I only do retrievals if the count of occupied bins is greater than 3, so just indicate missing values here
               N_Di_uncert[i_interval, :] = _MISSING_FLOAT
               #Just for diagnostic output
               sampling_var_est = missing_vals
               measurement_var_est = missing_vals
               total_var_est = missing_vals
      
        else:
            #No samples reported, should mean no observations of snow, so N_d and uncerts zero,
            #fitted N0 and lambda missing
            Di_uncert[i_interval, :] = 0.
            N_Di_mean[i_interval, :] = 0.
            N_Di_uncert[i_interval, :] = 0.
            N0_mm_fitted[i_interval] = _MISSING_FLOAT
            lambda_mm_fitted[i_interval] = _MISSING_FLOAT

    new_psd_data = psddata.PSDData(n_times = N_intervals,
                                   n_bins = psd_data.n_bins,
                                   fov_x = psd_data.fov_x,
                                   fov_y = psd_data.fov_y,
                                   f_dof = psd_data.f_dof,
                                   fps = psd_data.fps,
                                   dt = t_interval,
                                   frame_sampling_interval = psd_data.frame_sampling_interval,
                                   times_utc = sample_start_times,
                                   bin_lbounds = psd_data.bin_lbounds,
                                   bin_widths = psd_data.bin_widths,
                                   psd = N_Di_mean)

    #All intervals processed, return the results
    #return(sample_start_times, Di_mm, Di_uncert, N_Di_mean, N_Di_uncert)    
    #return(sample_start_times, Di_mm, Di_uncert, N_Di_mean, N_Di_uncert, N0_mm_fitted, lambda_mm_fitted)    
    return(new_psd_data, Di_uncert, N_Di_uncert, N0_mm_fitted, lambda_mm_fitted)


def dump(fpath, N_obs_actual, sample_start_times, Di_uncert, N_Di_mean, N_Di_uncert):
    #Example of writing results to a file for input into the retrieval code
    N_intervals, N_bins = N_Di_mean.shape

    with open(fpath, 'w') as f_out:
        for i_interval in range(N_intervals):
            f_out.write("%s" %(sample_start_times[i_interval]))
            for i_size in range(N_bins):
                 f_out.write("%15.5e" %(N_Di_mean[i_interval, i_size],))
            f_out.write("\n")
    
            f_out.write("%15d" %(N_obs_actual,))
            for i_size in range(N_bins):
                f_out.write("%15.5e" %(N_Di_uncert[i_interval, i_size],))
            f_out.write("\n")
    
            f_out.write("%15d" %(N_obs_actual,))
            for i_size in range(N_bins):
                f_out.write("%15.5e" %(Di_uncert[i_interval, i_size],))
            f_out.write("\n")
        f_out.close()



def best_fit_ND_exp(Di, N_Di, N_Di_sigma, diagnostics=False):
   #Get the indices of the significant, nonzero N_Di values
   indices_N_Di = numpy.where(numpy.logical_and(numpy.not_equal(N_Di, 0.), \
                                               numpy.greater_equal(Di, 0.00025)))[0]
   N_obs = len(indices_N_Di)
   N_Di_sub = N_Di[indices_N_Di]
   N_Di_sigma_sub = N_Di_sigma[indices_N_Di]
   Di_sub = Di[indices_N_Di]
   log_N_Di_sub = numpy.log(N_Di_sub)
   N_Di_var_sub = numpy.power(N_Di_sigma_sub,2.)

   #What's the variance of the log transformed data?
   #del^2(f(y)) = (d(f(y))/dy)^2*del^2(y)
   #d(ln(y))/dy = 1/y
   #So del^2(ln(y)) = (1/y)^2*del^2(y)
   log_N_Di_var_sub = (1./(log_N_Di_sub*log_N_Di_sub))*N_Di_var_sub

   #Set up the ODR fits
   exponential = scipy.odr.Model(fitfunc)
   N_Di_var_sub = numpy.power(N_Di_sigma_sub,2)
   weights = 1./log_N_Di_var_sub
   #weights = numpy.ones((N_obs,), dtype=float)
   fitdata = scipy.odr.Data(Di_sub, log_N_Di_sub, wd = 0., we=weights)
   myodr = scipy.odr.ODR(fitdata, exponential, beta0=[1000.,1.])
   myodr.set_job(fit_type=2)
   results = myodr.run()
   ier = results.info
   normed_chi_sq = numpy.sum(results.eps*results.eps/log_N_Di_var_sub)/N_obs
   if ier < 4:   #Probably good convergence, see ODRPACK user guide, page 38
      (log_N0_est, lambda_est) = results.beta
      N0_est = numpy.exp(log_N0_est)
      if diagnostics == True:
         indices = numpy.where(numpy.greater(N_Di, 0.))[0]
         N_Di_fitted = N0_est*numpy.exp(-Di*lambda_est)
         print("#Fitted")
         for i_bin in indices:
            print("%15.5e %15.5e %15.5e %15.5e" %(Di[i_bin], N_Di[i_bin], numpy.sqrt(log_N_Di_var_sub[i_bin]),N_Di_fitted[i_bin]))
   else:
      (N0_est, lamda_est) = (-999., -999.)

   return(N0_est, lambda_est)
