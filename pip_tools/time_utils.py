#!/usr/bin/env python3

"""Miscellaneous time utilities
"""

import sys
import numpy
import datetime

def decompose_dt64(dt64):
   #From https://stackoverflow.com/questions/13648774/get-year-month-or-day-from-numpy-datetime64/35281829#35281829
   Y, M, D, h, m, s = [dt64.astype(f"M8[{x}]") for x in "YMDhms"]
   year = Y+1970
   month = (M - Y) + 1
   day = (D - M) +1
   hour = (dt64 - D).astype("m8[h]")
   minute = (dt64 - h).astype("m8[m]")
   second = (dt64 - m).astype("m8[s]")
   us = (dt64 - s).astype("m8[us]")

   return (year, month, day, hour, minute, second, us)


def unixtime_to_datetime64(unixtime_data):
    # Convert variable containing Unix epoch time in seconds to a NumPy datetime64 array

    data = None
    if hasattr(unixtime_data, 'mask'):
        if unixtime_data.mask.any():
            sys.exit('Error:  Time data contains masked values')
        else:
            data = numpy.array(unixtime_data, dtype='datetime64[s]')
    else:
        data = numpy.array(unixtime_data, dtype='datetime64[s]')
    return data


def datetime64_to_unixtime(dt64_var):
    unixtime_data = (dt64_var - numpy.datetime64('1970-01-01T00:00:00.000000Z')) / numpy.timedelta64(1, 's')
    return unixtime_data


def seconds_to_HMS(total_seconds):
    # This is a better way than doing divisions, taking integers, then
    # calculating remainders via subtraction.  That method results in small imprecisions
    # when the seconds are near 0. or 60. that lead to errors in the calculated hours,
    # minutes and seconds.  This approach seems more resistant to that.  See
    # https://stackoverflow.com/questions/775049/python-time-seconds-to-hms

    minutes, seconds = divmod(total_seconds, 60)
    hours, minutes = divmod(minutes, 60)

    # Handle seconds vanishingly close to 60.
    if numpy.isclose(seconds, 60.):
        seconds = 0.
        minutes += 1
    if minutes >= 60:
        hours += 1
        minutes = minutes - 60

    return hours, minutes, seconds


def frac_day_to_HMS(frac_day):
    total_seconds = frac_day * 86400.
    hours, minutes, seconds = seconds_to_HMS(total_seconds)
    return hours, minutes, seconds


def datetime64_to_YMD(dt64_val):
    year = dt64_val.astype('datetime64[Y]').astype(int) + 1970
    month = dt64_val.astype('datetime64[M]').astype(int) % 12 + 1
    frac_day = ((dt64_val - dt64_val.astype('datetime64[M]')) /
                numpy.timedelta64(1, 'D'))

    day = frac_day.astype(int) + 1
    return year, month, day

def datetime64_to_string(dt64_val):
    x = dt64_val.astype(datetime.datetime)
    frac_seconds = x.microsecond/1.e6 + x.second
    timestring = '%4d-%02d-%02dT%02d:%02d:%06.3f' %(
                 x.year,
                 x.month,
                 x.day,
                 x.hour,
                 x.minute,
                 frac_seconds)
    return timestring

