#!/usr/bin/env python3

"""PSD data container class for Precipitation Imaging Package,
   Precipitation Video Imager and Snow Video Imager
"""

#--------------------------------------------------------------------------
# PSD data container class for observations from the Precipitation Imaging
# Package, Precipitation Video Imager, and Snow Video Imager
#
# Copyright (c) 2024 Norman Wood
#
# This file is part of the free software PIP_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
#
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details.
#
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#------------------------------------------------------------------------

import sys
import numpy
from . import diagnostics

_MISSING_FLOAT = -999.
_MISSING_INTEGER = -999

class PSDData:

   def __init__(self, n_times = None,
                      n_bins = None,
                      fov_x = None,
                      fov_y = None,
                      f_dof = None,
                      fps = None,
                      dt = None,
                      frame_sampling_interval = None,
                      times_utc = None,
                      bin_lbounds = None,
                      bin_widths = None,
                      psd = None):

      self.n_times = n_times
      self.n_bins = n_bins
      self.fov_x = fov_x
      self.fov_y = fov_y
      self.f_dof = f_dof
      self.fps = fps
      self.dt = dt
      self.frame_sampling_interval = frame_sampling_interval
      self.times_utc = times_utc
      self.bin_lbounds = bin_lbounds
      self.bin_widths = bin_widths
      self.psd = psd

def concatenate(psddata_list):

   """Concatenate a list of PSDData instances to produce a new PSDData instance
   """

   # These are fields that should be the same for all instances in psddata_list.
   # Check that they do not change
   fov_x_match = all([psddata_list[0].fov_x == getattr(x, 'fov_x') for x in psddata_list[1:]])
   fov_y_match = all([psddata_list[0].fov_y == getattr(x, 'fov_y') for x in psddata_list[1:]])
   f_dof_match = all([psddata_list[0].f_dof == getattr(x, 'f_dof') for x in psddata_list[1:]])
   fps_match = all([psddata_list[0].fps == getattr(x, 'fps') for x in psddata_list[1:]])
   dt_match = all([psddata_list[0].dt == getattr(x, 'dt') for x in psddata_list[1:]])
   frame_sampling_interval_match = all([psddata_list[0].frame_sampling_interval ==
                                   getattr(x, 'frame_sampling_interval') for x in psddata_list[1:]])
   lbounds_match = numpy.all([numpy.allclose(psddata_list[0].bin_lbounds,
                              getattr(x, 'bin_lbounds')) for x in psddata_list[1:]])
   widths_match = numpy.all([numpy.allclose(psddata_list[0].bin_widths,
                             getattr(x, 'bin_widths')) for x in psddata_list[1:]])
   if (lbounds_match and
       widths_match and
       fov_x_match and
       fov_y_match and
       f_dof_match and
       fps_match and
       dt_match and
       frame_sampling_interval_match):

       # Check that the observation times are uniformly increasing.
       times_utc = numpy.concatenate([getattr(x, 'times_utc') for x in psddata_list])
       times_utc_increase = numpy.all(numpy.diff(times_utc) > numpy.timedelta64(0, 's'))

       if times_utc_increase:
           psd_array = numpy.concatenate([getattr(x, 'psd') for x in psddata_list])

           n_times = times_utc.shape[0]
           n_bins = psd_array.shape[1]

           new_psddata = PSDData(n_bins = n_bins,
                                 n_times = n_times,
                                 fov_x = psddata_list[0].fov_x,
                                 fov_y = psddata_list[0].fov_y,
                                 f_dof = psddata_list[0].f_dof,
                                 fps = psddata_list[0].fps,
                                 dt = psddata_list[0].dt,
                                 frame_sampling_interval = psddata_list[0].frame_sampling_interval,
                                 bin_lbounds = psddata_list[0].bin_lbounds,
                                 bin_widths = psddata_list[0].bin_widths,
                                 times_utc = times_utc,
                                 psd = psd_array)
       else:
           sys.stderr.write('times_utc values not strictly increasing in PSDData.concatenate()\n')
           diagnostics.sequential_time_error_handler(psddata_list)
           assert 0
   else:
       sys.stderr.write('Inconsistencies in bin lbounds, widths, fov_x, fov_y, f_dof, fps, dt or frame_sampling_interval in PSDData.concatenate()\n')
       assert 0

   return new_psddata

