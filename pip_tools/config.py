#!/usr/bin/env python3

"""Set parameters for camera and datafiles from config file fpath_config

   For info only, these were the settings for the original SVI (see Newman et al., 2009)
   _FOV_XDIM = 32.                    #mm
   _FOV_YDIM = 24.                    #mm
   _F_dof = 117.                      #unitless factor for calculating depth of field
   _FPS = 56.3                        #frames per second
   _DT_s = 60                         #Observation interval, seconds (saved as a numpy.deltatime64
                                      #object, so must be an integer.  Use integer seconds for now.
   _Frame_sampling_interval = 1       #The number of frames skipped when computing particle counts
                                      #from successive frames so that no particle is counted
                                      #multiple times.
   _DSD_sheet_name = 'DSD_min_L_lin'  #This was the sheet for the Feret diameter DSDs
"""

# An immutable singleton class to hold configuration parameters realized by subclassing namedtuple
# Uses ideas from https://taube.web.illinois.edu/mus105/lectures/namedtuple/namedtuple.html

import sys
from collections import namedtuple

try:
   #Python3 version
   from configparser import ConfigParser
except ImportError:
   from ConfigParser import RawConfigParser as ConfigParser

_the_config = None

# Immutable backing store for configuration info
_ConfigData = namedtuple("_ConfigData", ["fov_xdim",
                                         "fov_ydim",
                                         "f_dof",
                                         "fps",
                                         "dt_s",
                                         "frame_sampling_interval",
                                         "dsd_sheet_name"])


class Config(_ConfigData):
   __slots__ = ()

   def __new__(cls, fpath_config=None):

      global _the_config

      # Process the configuration file only if _the_config has not been set yet
      if _the_config is None:

         if fpath_config is None:
            fpath_config = './pip.cfg'

         parser = ConfigParser()

         #The Python2 ConfigParser.RawConfigParser.read() and Python3 configparser.ConfigParser() methods
         #will silently ignore a missing config file and return an empty config list.  So check the length
         #of the returned list

         config = parser.read(fpath_config)
         if len(config) > 0:
            fov_xdim = parser.getfloat('Main', 'FOV_XDIM')
            fov_ydim = parser.getfloat('Main', 'FOV_YDIM')
            f_dof = parser.getfloat('Main', 'F_dof')
            fps = parser.getfloat('Main', 'FPS')
            dt_s = parser.getint('Main', 'DT_s')
            frame_sampling_interval = parser.getint('Main', 'Frame_sampling_interval')
            dsd_sheet_name = parser.get('Main', 'DSD_sheet_name')
            self = super().__new__(cls, fov_xdim,
                                        fov_ydim,
                                        f_dof,
                                        fps,
                                        dt_s,
                                        frame_sampling_interval,
                                        dsd_sheet_name)
            _the_config = self
         else:
            try:
               fp = open(fpath_config)
               fp.close()
               sys.stderr.write('SVI/PVI/PIP configuration file found at %s, but not read by parser\n' %(fpath_config,))
               assert 0
            except FileNotFoundError:
               sys.stderr.write('SVI/PVI/PIP configuration file not found at %s\n' %(fpath_config,))
               raise

      return _the_config

def run_test():
   config1 = Config(fpath_config='./pip.cfg')
   config2 = Config(fpath_config='./pip.cfg')
   print(config1 == config2)

   try:
      config1.fov_xdim = 0.
   except AttributeError as e:
      print(e)

   try:
      config1.dummy = None
   except AttributeError as e:
      print(e)

   try:
      config1[0] = 0.
   except TypeError as e:
      print(e)

if __name__ == '__main__':
   run_test()
