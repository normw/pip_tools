#!/usr/bin/env python


"""Diagnostic utilities for data structures
"""

#--------------------------------------------------------------------------
# Diagnostic utilities for data structures
#
# Copyright (c) 2017 Norman Wood
#
# This file is part of the free software GV_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------




import sys
import numpy
from . import time_utils

def _sequential_time_check(data_tuple):
    time_diffs = numpy.diff(data_tuple.times_utc)
    mask = numpy.less_equal(time_diffs, numpy.timedelta64(0, 's'))
    if numpy.any(mask):
        indices = numpy.where(mask)[0]
        for idx in indices:
            sys.stderr.write('%10d %s %s %s\n' %(
                idx,
                time_diffs[idx].astype('timedelta64[m]'),
                time_utils.datetime64_to_string(data_tuple.times_utc[idx]),
                time_utils.datetime64_to_string(data_tuple.times_utc[idx+1])))
    return

def sequential_time_error_handler(data_tuple_list):
    """Provide diagnostics for time variables that are not
    strictly increasing.
    """
    #Check up to the next-to-last data tuple, and the time
    #transition between the current and next data tuple
    for i_tuple, x in enumerate(data_tuple_list[:-1]):
        #Check within the tuple
        _sequential_time_check(x)
        #Check the transition
        x_next = data_tuple_list[i_tuple+1]
        time_diff = x_next.times_utc[0] - x.times_utc[-1]
        if numpy.less_equal(time_diff, numpy.timedelta64(0, 's')):
            sys.stderr.write('(transition) %s %s %s\n' %(
                time_diff.astype('timedelta64[m]'),
                time_utils.datetime64_to_string(x.times_utc[-1]),
                time_utils.datetime64_to_string(x_next.times_utc[0])))
            sys.stderr.write('From data_tuple_list item %d to %d\n' %(i_tuple, i_tuple+1))
    #Finally check the last data tuple
    x = data_tuple_list[-1]
    _sequential_time_check(x)
