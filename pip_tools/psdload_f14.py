#!/usr/bin/env python3

"""Loader for f_1_4 ASCII PSD data
"""

import numpy
from . import psddata
from . import time_utils

_I_LINE_REV = 1
_I_LINE_YMD = 5
_I_LINE_STA_ID = 7
_I_LINE_BIN_LBOUNDS = 9
_I_LINE_BIN_WIDTHS = 10
_I_LINE_DATA_START = 12


def _parse_data_line(line):
   parts = line.split()
   if parts[0] != '-99':
      frac_day = float(parts[0])
      hh = int(parts[1])
      mm = int(parts[2])
      N_particles = int(parts[3])
      psd_vals = numpy.array([float(x) for x in parts[5:]], dtype='float')
      return_val = (frac_day, hh, mm, N_particles, psd_vals)
   else:
      return_val = None
   return(return_val)

def _parse_bin_lbounds(line):
   parts = line.split()
   lbounds_vals = numpy.array([float(x) for x in parts[5:]], dtype='float')
   return lbounds_vals

def _parse_bin_widths(line):
   parts = line.split()
   widths_vals = numpy.array([float(x) for x in parts[5:]], dtype='float')
   return widths_vals

def _parse_date_line(line):
   ymd_list = [int(x) for x in line.split()]
   return(ymd_list)

def load(cfg, fpath):
   with open(fpath, 'r') as f_in:
      lines = f_in.readlines()

   [year, month, day] = _parse_date_line(lines[_I_LINE_YMD])
   bin_lbounds = _parse_bin_lbounds(lines[_I_LINE_BIN_LBOUNDS])
   bin_widths = _parse_bin_widths(lines[_I_LINE_BIN_WIDTHS])
   psd_vals_list = []
   times_utc_list = []
   
   for line in lines[_I_LINE_DATA_START:]:
      result = _parse_data_line(line)
      if result is not None:
         (frac_day, hh, mm, N_particles, psd_vals) = result
         psd_vals_list.append(psd_vals)
         hours, minutes, seconds = time_utils.frac_day_to_HMS(frac_day)
         time_string = '%4d-%02d-%02dT%02d:%02d:%09.6f' %(year, month, day, hours, minutes, seconds)
         times_utc_list.append(numpy.datetime64(time_string))

   N_samples = len(times_utc_list)
   if N_samples > 0:
      times_utc_arr = numpy.array(times_utc_list)
      psd_vals_arr = numpy.array(psd_vals_list)

      psd_data = psddata.PSDData(n_times = N_samples,
                                 n_bins = bin_lbounds.shape[0],
                                 fov_x = cfg.fov_xdim,
                                 fov_y = cfg.fov_ydim,
                                 f_dof = cfg.f_dof,
                                 fps = cfg.fps,
                                 dt = numpy.timedelta64(cfg.dt_s, 's'),
                                 frame_sampling_interval = cfg.frame_sampling_interval,
                                 times_utc = times_utc_arr,
                                 bin_lbounds = bin_lbounds,
                                 bin_widths = bin_widths,
                                 psd = psd_vals_arr)
   else:
      psd_data = None
   return psd_data



def test():
   from . import config
   from . import psddata
   cfg = config.Config(fpath_config='./pip.cfg')

   fpaths = ['/thermal/norm/Datasets/BlizEx_v0/PIP/2021_OAKV/PIP_3/f_1_4_DSD_Tables_ascii/010202201042350_01_dsd.dat',
             '/thermal/norm/Datasets/BlizEx_v0/PIP/2021_OAKV/PIP_3/f_1_4_DSD_Tables_ascii/010202201052350_01_dsd.dat']

   psd_data_list = []
   for fpath in fpaths:
      psd_data = load(cfg, fpath)
      if psd_data is not None:
         psd_data_list.append(psd_data)

   psd_data = psddata.concatenate(psd_data_list)

   print(psd_data.times_utc.shape)
