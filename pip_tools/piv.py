#!/usr/bin/env python3
import numpy
import matplotlib.figure
import matplotlib.backends.backend_agg

# Module containing tools for working with .piv files

def extract(f_in, n_bytes, offset):
  bytestr = f_in.read(n_bytes)
  indices = []
  values = []
  for i in range(0, n_bytes, 2):
      indices.append(int.from_bytes(bytestr[i:i+2], 'big', signed=True))
  for i in range(1, n_bytes, 2):
      values.append(int.from_bytes(bytestr[i:i+2], 'big', signed=True))
  row_idxs = offset + numpy.floor_divide(numpy.array(indices, dtype=numpy.short), 640)
  column_idxs = indices - 640*(numpy.floor_divide(numpy.array(indices, dtype=numpy.short), 640))
  values = numpy.array(values, dtype=numpy.ubyte)
   
  return(row_idxs, column_idxs, values)

def parse(header_str):
   condition = int.from_bytes(header_str[0:2], 'big', signed=True)
   i_frame = int.from_bytes(header_str[2:4], 'big', signed=True)
   minute = int.from_bytes(header_str[4:6], 'big', signed=True)
   second = int.from_bytes(header_str[6:8], 'big', signed=True)

   return condition, i_frame, minute, second

def extract_frame(header_str, f_in):
   #Default frame is all white
   image_frame = 255*numpy.ones((240,640), dtype=numpy.ubyte)

   #Insert any gray pixels
   #Process slice A
   count = int.from_bytes(header_str[8:10], 'big', signed=True)
   offset = 0
   n_bytes = 4*count
   row_idxs_A, column_idxs_A, values_A = extract(f_in, n_bytes, offset)

   for idx in range(row_idxs_A.shape[0]):
      image_frame[row_idxs_A[idx], column_idxs_A[idx]] = values_A[idx]
      print('%8d %8d %8d:  %8d' %(row_idxs_A[idx], column_idxs_A[idx], values_A[idx],
                                  image_frame[row_idxs_A[idx], column_idxs_A[idx]]))

   #Process slice B
   count = int.from_bytes(header_str[10:12], 'big', signed=True)
   offset = 40
   n_bytes = 4*count
   row_idxs_B, column_idxs_B, values_B = extract(f_in, n_bytes, offset)

   for idx in range(row_idxs_B.shape[0]):
      image_frame[row_idxs_B[idx], column_idxs_B[idx]] = values_B[idx]
      print('%8d %8d %8d' %(row_idxs_B[idx], column_idxs_B[idx], values_B[idx]))

   #Process slice C
   count = int.from_bytes(header_str[12:14], 'big', signed=True)
   offset = 80
   n_bytes = 4*count
   row_idxs_C, column_idxs_C, values_C = extract(f_in, n_bytes, offset)

   for idx in range(row_idxs_C.shape[0]):
      image_frame[row_idxs_C[idx], column_idxs_C[idx]] = values_C[idx]
      print('%8d %8d %8d' %(row_idxs_C[idx], column_idxs_C[idx], values_C[idx]))

   #Process slice D
   count = int.from_bytes(header_str[14:16], 'big', signed=True)
   offset = 120
   n_bytes = 4*count
   row_idxs_D, column_idxs_D, values_D = extract(f_in, n_bytes, offset)

   for idx in range(row_idxs_D.shape[0]):
      image_frame[row_idxs_D[idx], column_idxs_D[idx]] = values_D[idx]
      print('%8d %8d %8d' %(row_idxs_D[idx], column_idxs_D[idx], values_D[idx]))

   #Process slice E
   count = int.from_bytes(header_str[16:18], 'big', signed=True)
   offset = 160
   n_bytes = 4*count
   row_idxs_E, column_idxs_E, values_E = extract(f_in, n_bytes, offset)

   for idx in range(row_idxs_E.shape[0]):
      image_frame[row_idxs_E[idx], column_idxs_E[idx]] = values_E[idx]
      print('%8d %8d %8d' %(row_idxs_E[idx], column_idxs_E[idx], values_E[idx]))

   #Process slice F
   count = int.from_bytes(header_str[18:20], 'big', signed=True)
   offset = 200 
   n_bytes = 4*count
   row_idxs_F, column_idxs_F, values_F = extract(f_in, n_bytes, offset)

   for idx in range(row_idxs_F.shape[0]):
      image_frame[row_idxs_F[idx], column_idxs_F[idx]] = values_F[idx]
      print('%8d %8d %8d' %(row_idxs_F[idx], column_idxs_F[idx], values_F[idx]))

   return image_frame

def load(piv_fpath):

   with open(piv_fpath, 'rb') as f_in:
      #header = piv_fpath(20*16)
      file_header = f_in.read(40)
      while(True):
         header_str = f_in.read(40)
         if len(header_str) == 0:
            print("End of file")
            break
         else:
            image_frame = None
            (condition, i_frame, minutes, seconds) = parse(header_str)
            if condition == -199:
               # Precip, continue
               pass
            elif condition == -187:
               image_frame = extract_frame(header_str, f_in)
               if image_frame is not None:
                  #Do something with it
                  print('%d:  Found a precip frame' %(condition))
               else:
                  print('Error, condition is -187 but image_frame is None')
                  assert 0
            else:
               print('Condition %d is not recognized')
               assert 0  
   return(None)

if __name__ == '__main__':
   fpath = './Test_data/0102016123001500.piv'
   #image_frame = load(fpath)
   load(fpath)
   #if image_frame is not None:

   #   fig = matplotlib.figure.Figure(figsize=(12,18))
   #   canvas = matplotlib.backends.backend_agg.FigureCanvasAgg(fig)
   #   ax1 = fig.add_subplot(111)
   #   plt = ax1.pcolor(image_frame, cmap='gray', vmin=0, vmax=255)
   #   canvas.print_figure('test_piv.png')
   #   #for i_row in range(image_frame.shape[0]):
   #   #   print(image_frame[i_row,:])


