#!/usr/bin/env python


"""Structure and methods for PIP_3_f_2_2_Velocity_Tables files
"""

#--------------------------------------------------------------------------
# Structure and methods for PIP_3_f_2_2_Velocity_Tables files
#
# Copyright (c) 2019 Norman Wood
#
# This file is part of the free software PIP_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


import sys
import numpy
import collections
import zipfile

#Fallspeed data from f_2_2_Velocity_Tables files, used for both the _v1.dat
#and _v2.dat files


PIP2Velocity = collections.namedtuple('PIP2Velocity', ['PIP_rev',
                                                       'instrument_number',
                                                       'station',
                                                       'record_number',
                                                       'times',
                                                       'particle_id',
                                                       'waddell_diam',
                                                       'vel_h',
                                                       'vel_v'])



def _load_single(fpath):

    #If fpath is a zip file, use zipfile module to open it
    #Otherwise open it as an ascii file and read it
    if zipfile.is_zipfile(fpath):
        f_ptr=zipfile.ZipFile(fpath, 'r')
        fnames = f_ptr.namelist()
        #Should always contain a single file
        if len(fnames) > 1:
            #Error condition
            sys.exit("In pip_f22._load_single, zipfile contains more than one datafile.")
        f_in = f_ptr.open(fnames[0])
        lines = f_in.readlines()
        f_in.close()
        f_ptr.close()
    else:
        with open(fpath, 'r') as f_in:
            lines = f_in.readlines()

    data_tuple = None

    PIP_rev = lines[1].split()[0]
    instrument_number = lines[3].split()[0]
    yr, mon, day, hr = map(int, lines[5].split())
    station = lines[7].split()[0]

    datetime_list = []
    particle_id_list = []
    waddell_diam_list = []
    vel_h_list = []
    vel_v_list = []
    record_number_list = []
    for line in lines[9:]:
        parts = line.split()
        record_number = int(parts[0])
        if record_number != -99:
            particle_id = int(parts[1])
            waddell_diam = float(parts[2])
            vel_h = float(parts[3])
            vel_v = float(parts[4])
            minute = int(parts[5])
            datetimestring = '%4d-%02d-%02dT%02d:%02d:00' %(yr, mon, day, hr, minute)
            datetime = numpy.datetime64(datetimestring)
            datetime_list.append(datetime)
            particle_id_list.append(particle_id)
            waddell_diam_list.append(waddell_diam)
            vel_h_list.append(vel_h)
            vel_v_list.append(vel_v)
            record_number_list.append(record_number)
        else:
            pass


    if len(record_number_list) > 0:
        data_tuple = PIP2Velocity(PIP_rev = PIP_rev,
                                  instrument_number = instrument_number,
                                  station = station,
                                  record_number = numpy.array(record_number_list),
                                  times = numpy.array(datetime_list),
                                  particle_id = numpy.array(particle_id_list), 
                                  waddell_diam = numpy.array(waddell_diam_list),
                                  vel_h = numpy.array(vel_h_list),
                                  vel_v = numpy.array(vel_v_list))

    return data_tuple

def _variable_concatenate(varname, data_tuple_list):
    #for i_tuple, x in enumerate(data_tuple_list):
    #    if x is not None:
    #        tmp = getattr(x, varname)
    #        print(i_tuple, type(tmp), type(tmp[0]))
    #        print(varname, tmp)
    x = numpy.concatenate([getattr(x, varname) for x in data_tuple_list if x is not None])
    return x

def _concatenate(fpaths):

    data_tuple = None

    data_tuple_list = []
    for fpath in fpaths:
        x = _load_single(fpath)
        if x is not None:
            data_tuple_list.append(x)

    if len(data_tuple_list) > 0:
        try:
            times = _variable_concatenate('times', data_tuple_list)
        except TypeError:
            print(fpaths[6])
            assert 0

        #Make sure that times are strictly increasing
        if not numpy.all(numpy.diff(times) >= numpy.timedelta64(0, 's')):
            #Bail out
            sys.stderr.write('times values not strictly increasing in pip_f22._concatenate()\n')
            sys.stderr.write('Check whether fpaths list is sequential.\n')
            sys.exit()
        #Make sure the instrument number, station, and PIP_rev are all the same
        for x in data_tuple_list[1:]:
            if not x.station == data_tuple_list[0].station:
                raise ValueError('Value of station should not change.')
            if not x.instrument_number == data_tuple_list[0].instrument_number:
                raise ValueError('Change in value of instrument_number is not supported.')
            if not x.PIP_rev == data_tuple_list[0].PIP_rev:
                raise ValueError('Change in value of PIP_rev is not supported.')

        data_tuple = PIP2Velocity(PIP_rev = data_tuple_list[0].PIP_rev,
                                  instrument_number = data_tuple_list[0].instrument_number,
                                  station = data_tuple_list[0].station,
                                  record_number = _variable_concatenate('record_number', data_tuple_list),
                                  times = times,
                                  particle_id = _variable_concatenate('particle_id', data_tuple_list),
                                  waddell_diam = _variable_concatenate('waddell_diam', data_tuple_list),
                                  vel_h = _variable_concatenate('vel_h', data_tuple_list),
                                  vel_v = _variable_concatenate('vel_v', data_tuple_list))
    
    return data_tuple


def load(fpaths):

    data_tuple = None
    if not isinstance(fpaths, (list, tuple, numpy.ndarray)):
        data_tuple =  _load_single(fpaths)
    else:
        data_tuple = _concatenate(fpaths)
    return data_tuple


