#!/usr/bin/env python3

"""Loader for f_1_3 Excel PSD data
"""

#--------------------------------------------------------------------------
# Structure and methods for f_1_3 DSD data in Excel spreadsheets
# for Precipitation Imaging Package observations (may also work for
# Precipitation Video Imager and Snow Video Imager, but needs to
# be tested).
#
# Copyright (c) 2024 Norman Wood
#
# This file is part of the free software PIP_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
#
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details.
#
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#------------------------------------------------------------------------

import numpy
import xlrd
from . import time_utils
from . import psddata

#xlrd cell ctypes valid for conversion to float
_XL_CELL_NUMBER = 2

_MISSING_FLOAT = -999.
_MISSING_INTEGER = -999


def _cell_list_to_numpy_float(cell_list):
   N_cells = len(cell_list)
   val_array = numpy.asarray([x.value if x.ctype in [_XL_CELL_NUMBER,] else _MISSING_FLOAT for x in cell_list], dtype=float)
   return val_array

def load(cfg, fpath):

   wb = xlrd.open_workbook(fpath, on_demand=True)

   #Get date information from Header sheet
   header_datasheet = wb.sheet_by_name('Header')
   datestring = header_datasheet.cell_value(3, 1)[3:11]
   year = int(datestring[0:4])
   month = int(datestring[4:6])
   day = int(datestring[6:8])

   psd_datasheet = wb.sheet_by_name(cfg.dsd_sheet_name)

   N_rows = psd_datasheet.nrows
   N_cols = psd_datasheet.ncols
   N_bins = N_cols - 2
   N_samples = N_rows - 3

   #Calls to psd_datasheet.row return a list of xlrd Cells
   size_bins_lbound_row = psd_datasheet.row(0)
   size_bins_widths_row = psd_datasheet.row(1)

   size_bins_lbound = _cell_list_to_numpy_float(size_bins_lbound_row[1:-1])
   size_bins_width = _cell_list_to_numpy_float(size_bins_widths_row[1:-1])
   size_bins_ubound = size_bins_lbound + size_bins_width

   psd_array = numpy.empty((N_samples, N_bins), dtype=float)
   times_array = numpy.empty((N_samples,), dtype='datetime64[us]')

   for i_sample in range(N_samples):
       i_row = i_sample + 3

       row = psd_datasheet.row(i_row)
       #Process this row only if the first element can be converted
       #to a valid float.  Otherwise skip this row and continue
       #to the next
       if row[0].ctype in [_XL_CELL_NUMBER,]:
           time_frac_day_utc = row[0].value
       else:
           continue
       hours, minutes, seconds = time_utils.frac_day_to_HMS(time_frac_day_utc)
       time_string = '%4d-%02d-%02dT%02d:%02d:%09.6f' %(year, month, day, hours, minutes, seconds)
       tmp = numpy.datetime64(time_string)
       times_array[i_sample] = numpy.datetime64(time_string)
       psd_vals = _cell_list_to_numpy_float(row[1:-1])
       psd_array[i_sample,:] = psd_vals[:]


   psd_data = psddata.PSDData(n_times = N_samples,
                              n_bins = N_bins,
                              fov_x = cfg.fov_xdim,
                              fov_y = cfg.fov_ydim,
                              f_dof = cfg.f_dof,
                              fps = cfg.fps,
                              dt = numpy.timedelta64(cfg.dt_s, 's'),
                              frame_sampling_interval = cfg.frame_sampling_interval,
                              times_utc = times_array,
                              bin_lbounds = size_bins_lbound,
                              bin_widths = size_bins_width,
                              psd = psd_array)

   wb.release_resources()
   del wb

   return psd_data



def test():
   from . import config
   from . import psddata

   cfg = config.Config(fpath_config='./pip.cfg')

   fpaths = ['/thermal/norm/Datasets/BlizEx_v0/PIP/2021_OAKV/PIP_3/f_1_3_DSD_Tables_xls/010202201042350_01_dsd.xls',
             '/thermal/norm/Datasets/BlizEx_v0/PIP/2021_OAKV/PIP_3/f_1_3_DSD_Tables_xls/010202201052350_01_dsd.xls']

   psd_data_list = []
   for fpath in fpaths:
      psd_data = load(cfg, fpath)
      if psd_data is not None:
         psd_data_list.append(psd_data)
 
   psd_data = psddata.concatenate(psd_data_list)

   print(psd_data.times_utc.shape)


if __name__ == '__main__':
    test()
