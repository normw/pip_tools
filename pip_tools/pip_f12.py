#!/usr/bin/env python3


"""Structure and methods for PIP f_1_2 single-particle data
"""

#--------------------------------------------------------------------------
# Structure and methods for PIP f_1_2 single-particle data
#
# Copyright (c) 2019 Norman Wood
#
# This file is part of the free software PIP_tools:  you can use,
# redistribute and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


import sys
import numpy
import collections
import zipfile


#Note that from Documentation_Particle_Table_a.xlsx from the
#A_PIP_2_Documentation_rev00.zip file provided to us by Larry,
#the following variables that appear in the PIP_2 tables are not
#used:
#IciTime
#FT
#Box_x
#Box_y

#Descriptions of the remaining variables, verbatim from the xlsx file

#Single-particle data from PIP f_1_2_Particle_Tables ascii files.
#This code should likely also apply to the subsampled data files
#(i. e., f_1_1_Particle_Tables_subsample).

_FRAME_COUNTER_DIVISOR = 32767

PIP2SingleParticle = collections.namedtuple('PIP2SingleParticle', ['PIP_rev',
                                                                   'instrument_number',
                                                                   'station',
                                                                   'record_number',
                                                                   'record_class',
                                                                   'times',
                                                                   'ReadHere',
                                                                   'frame_number',
                                                                   'x_centroid',
                                                                   'y_centroid',
                                                                   'hole_count',
                                                                   'ellipse_major',
                                                                   'ellipse_minor',
                                                                   'total_area',
                                                                   'waddell_diam',
                                                                   'max_feret_diam',
                                                                   'particle_orientation_angle',
                                                                   'rect_len_long',
                                                                   'rect_len_short',
                                                                   'hydraulic_radius',
                                                                   'x_coord_left',
                                                                   'x_coord_right',
                                                                   'y_coord_top',
                                                                   'y_coord_bottom',
                                                                   'minimum_gray',
                                                                   'frame_counter_divisor',
                                                                   'frame_counter_quotient',
                                                                   'frame_counter_remainder'])





def _load_single(fpath):

    data_tuple = None

    #If fpath is a zip file, use zipfile module to open it
    #Otherwise open it as an ascii file and read it
    if zipfile.is_zipfile(fpath):
        f_ptr=zipfile.ZipFile(fpath, 'r')
        fnames = f_ptr.namelist()
        #Should always contain a single file
        if len(fnames) > 1:
            #Error condition
            sys.exit("In pip_f12._load_single, zipfile contains more than one datafile.")
        f_in = f_ptr.open(fnames[0])
        lines = f_in.readlines()
        f_in.close()
        f_ptr.close()
    else:
        with open(fpath, 'r') as f_in:
            lines = f_in.readlines()

    PIP_rev = lines[1].split()[0]
    instrument_number = lines[3].split()[0]
    yr, mon, day = map(int, lines[5].split())   #Don't use this since each data line contains year, month, day, hour, minute, second
    station = lines[7].split()[0]

    record_number_list = []
    record_class_list = []
    times_list = []
    ReadHere_list = []
    frame_number_list = []
    x_centroid_list = []
    y_centroid_list = []
    hole_count_list = []
    ellipse_major_list = []
    ellipse_minor_list = []
    total_area_list = []
    waddell_diam_list = []
    max_feret_diam_list = []
    particle_orientation_angle_list = []
    rect_len_long_list = []
    rect_len_short_list = []
    hydraulic_radius_list = []
    x_coord_left_list = []
    x_coord_right_list = []
    y_coord_top_list = []
    y_coord_bottom_list = []
    minimum_gray_list = []
    frame_counter_quotient_list = []
    frame_counter_remainder_list = []

    for line in lines[10:]:
        parts = line.split()
        record_class = int(parts[1])
        if record_class == 0:
            #This is single-particle data, save it
            record_number = int(parts[0])
            #print(record_number)
            ReadHere = int(parts[5])
            yr = int(parts[6])
            mon = int(parts[7])
            day = int(parts[8])
            hr = int(parts[9])
            minute = int(parts[10])
            sec = int(parts[11])
            datetimestring = '%4d-%02d-%02dT%02d:%02d:%02d' %(yr, mon, day, hr, minute, sec)
            datetime = numpy.datetime64(datetimestring)
            frame_number = int(parts[13])
            x_centroid = float(parts[14])
            y_centroid = float(parts[15])
            hole_count = int(parts[16])
            ellipse_major_axis = float(parts[17])
            ellipse_minor_axis = float(parts[18])
            total_area = float(parts[19])
            waddell_diam = float(parts[20])
            max_feret_diam = float(parts[21])
            particle_orientation_angle = float(parts[22])
            rect_len_long = float(parts[23])
            rect_len_short = float(parts[24])
            hydraulic_radius = float(parts[25])
            x_coord_left = int(parts[27])
            x_coord_right = int(parts[28])
            y_coord_top = int(parts[29])
            y_coord_bottom = int(parts[30])
            minimum_gray = int(parts[33])
            frame_counter_quotient = int(parts[34])
            frame_counter_remainder = int(parts[35])

            record_number_list.append(record_number)
            record_class_list.append(record_class)
            ReadHere_list.append(ReadHere)
            times_list.append(datetime)
            frame_number_list.append(frame_number)
            x_centroid_list.append(x_centroid)
            y_centroid_list.append(y_centroid)
            hole_count_list.append(hole_count)
            ellipse_major_list.append(ellipse_major_axis)
            ellipse_minor_list.append(ellipse_minor_axis)
            total_area_list.append(total_area)
            waddell_diam_list.append(waddell_diam)
            max_feret_diam_list.append(max_feret_diam)
            particle_orientation_angle_list.append(particle_orientation_angle)
            rect_len_long_list.append(rect_len_long)
            rect_len_short_list.append(rect_len_short)
            hydraulic_radius_list.append(hydraulic_radius)
            x_coord_left_list.append(x_coord_left)
            x_coord_right_list.append(x_coord_right)
            y_coord_top_list.append(y_coord_top)
            y_coord_bottom_list.append(y_coord_bottom)
            minimum_gray_list.append(minimum_gray)
            frame_counter_quotient_list.append(frame_counter_quotient)
            frame_counter_remainder_list.append(frame_counter_remainder)


        elif record_class == 1:
            #This is one-second summary data
            pass
        elif record_class == 2:
            #This is one-minute summary data
            pass
        elif record_class == -99:
            #End of file condition
            pass

    if len(record_number_list) > 0:
        data_tuple = PIP2SingleParticle(PIP_rev = PIP_rev,
                                        instrument_number = instrument_number,
                                        station = station,
                                        record_number = numpy.array(record_number_list),
                                        record_class = numpy.array(record_class_list),
                                        times = numpy.array(times_list),
                                        ReadHere = numpy.array(ReadHere_list),
                                        frame_number = numpy.array(frame_number_list),
                                        x_centroid = numpy.array(x_centroid_list),
                                        y_centroid = numpy.array(y_centroid_list),
                                        hole_count = numpy.array(hole_count_list),
                                        ellipse_major = numpy.array(ellipse_major_list),
                                        ellipse_minor = numpy.array(ellipse_minor_list),
                                        total_area = numpy.array(total_area_list),
                                        waddell_diam = numpy.array(waddell_diam_list),
                                        max_feret_diam = numpy.array(max_feret_diam_list),
                                        particle_orientation_angle = numpy.array(particle_orientation_angle_list),
                                        rect_len_long = numpy.array(rect_len_long_list),
                                        rect_len_short = numpy.array(rect_len_short_list),
                                        hydraulic_radius = numpy.array(hydraulic_radius_list),
                                        x_coord_left = numpy.array(x_coord_left_list),
                                        x_coord_right = numpy.array(x_coord_right_list),
                                        y_coord_top = numpy.array(y_coord_top_list),
                                        y_coord_bottom = numpy.array(y_coord_bottom_list),
                                        minimum_gray = numpy.array(minimum_gray_list),
                                        frame_counter_divisor = _FRAME_COUNTER_DIVISOR,
                                        frame_counter_quotient = numpy.array(frame_counter_quotient_list),
                                        frame_counter_remainder = numpy.array(frame_counter_remainder_list))

    return data_tuple

def _variable_concatenate(varname, data_tuple_list):
    x = numpy.concatenate([getattr(x, varname) for x in data_tuple_list if x is not None])
    return x

def _concatenate(fpaths):

    data_tuple = None

    data_tuple_list = []
    for fpath in fpaths:
        x = _load_single(fpath)
        if x is not None:
            data_tuple_list.append(x)

    if len(data_tuple_list) > 0:
        times = _variable_concatenate('times', data_tuple_list)

        #Make sure that times are strictly increasing
        if not numpy.all(numpy.diff(times) >= numpy.timedelta64(0, 's')):
            #Bail out
            sys.stderr.write('times values not strictly increasing in pip_f12._concatenate()\n')
            sys.stderr.write('Check whether fpaths list is sequential.\n')
            sys.exit()

        #Make sure the instrument number, station, and PIP_rev are all the same
        for x in data_tuple_list[1:]:
            if not x.station == data_tuple_list[0].station:
                raise ValueError('Value of station should not change.')
            if not x.instrument_number == data_tuple_list[0].instrument_number:
                raise ValueError('Change in value of instrument_number is not supported.')
            if not x.PIP_rev == data_tuple_list[0].PIP_rev:
                raise ValueError('Change in value of PIP_rev is not supported.')
            if not x.frame_counter_divisor == data_tuple_list[0].frame_counter_divisor:
                raise ValueError('Change in vaue of frame_counter_divisor is not supported')
   
        data_tuple = PIP2SingleParticle(PIP_rev = data_tuple_list[0].PIP_rev,
                                        instrument_number = data_tuple_list[0].instrument_number,
                                        station = data_tuple_list[0].station,
                                        record_number = _variable_concatenate('record_number', data_tuple_list),
                                        record_class = _variable_concatenate('record_class', data_tuple_list),
                                        times = _variable_concatenate('times', data_tuple_list),
                                        ReadHere = _variable_concatenate('ReadHere', data_tuple_list),
                                        frame_number = _variable_concatenate('frame_number', data_tuple_list),
                                        x_centroid = _variable_concatenate('x_centroid', data_tuple_list),
                                        y_centroid = _variable_concatenate('y_centroid', data_tuple_list),
                                        hole_count = _variable_concatenate('hole_count', data_tuple_list),
                                        ellipse_major = _variable_concatenate('ellipse_major', data_tuple_list),
                                        ellipse_minor = _variable_concatenate('ellipse_minor', data_tuple_list),
                                        total_area = _variable_concatenate('total_area', data_tuple_list),
                                        waddell_diam = _variable_concatenate('waddell_diam', data_tuple_list),
                                        max_feret_diam = _variable_concatenate('max_feret_diam', data_tuple_list),
                                        particle_orientation_angle = _variable_concatenate('particle_orientation_angle', data_tuple_list),
                                        rect_len_long = _variable_concatenate('rect_len_long', data_tuple_list),
                                        rect_len_short = _variable_concatenate('rect_len_short', data_tuple_list),
                                        hydraulic_radius = _variable_concatenate('hydraulic_radius', data_tuple_list),
                                        x_coord_left = _variable_concatenate('x_coord_left', data_tuple_list),
                                        x_coord_right = _variable_concatenate('x_coord_right', data_tuple_list),
                                        y_coord_top = _variable_concatenate('y_coord_top', data_tuple_list),
                                        y_coord_bottom = _variable_concatenate('y_coord_bottom', data_tuple_list),
                                        minimum_gray = _variable_concatenate('minimum_gray', data_tuple_list),
                                        frame_counter_divisor = data_tuple_list[0].frame_counter_divisor,
                                        frame_counter_quotient = _variable_concatenate('frame_counter_quotient', data_tuple_list),
                                        frame_counter_remainder = _variable_concatenate('frame_counter_remainder', data_tuple_list))
    return data_tuple

def load(fpaths):

    data_tuple = None
    if not isinstance(fpaths, (list, tuple, numpy.ndarray)):
        data_tuple = _load_single(fpaths)
    else:
        data_tuple = _concatenate(fpaths)
    return data_tuple
