from setuptools import setup, find_packages

with open('README.txt', 'rb') as f:
    long_descr = f.read().decode('utf-8')

setup(name='pip_tools',
    packages=find_packages('.', exclude=['test', 'tests', 'Test_data']),
    package_dir={'':'.'},
    description = 'Tools for working with SVI/PVI/PIP observations.',
    version='0.0',
    long_description=long_descr,
    author = 'Norm Wood',
    author_email = 'normw013@fastmail.fm',
    url = '',
    license = 'BSD-3-Clause',
    zip_safe = False,
    include_package_data = True,
   )

